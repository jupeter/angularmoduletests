'use strict';

/* jasmine specs for angular.module.js go here */

describe('myModule', function() {

    // load application module (`greetMod`) then load a special
    // test module which overrides `$window` with a mock version,
    // so that calling `window.alert()` will not block the test
    // runner with a real alert box.
    beforeEach(module('myModule', function($provide) {
        $provide.value('$window', {
            alert: jasmine.createSpy('alert')
        });
    }));

    // test specify
    it('check if service work with default instance', inject(function(myService) {
        expect(myService).toBe(123);
    }));

});

describe('myModule2', function() {

    // load application module (`greetMod`) then load a special
    // test module which overrides `$window` with a mock version,
    // so that calling `window.alert()` will not block the test
    // runner with a real alert box.
    beforeEach(module('myModule2', function($provide) {
        $provide.value('$window', {
            alert: jasmine.createSpy('alert')
        });
    }));

    it('check if service work, when we load module dynamic', function() {
        var myModule2 = angular.module('myModule2');

        var alertSpy = jasmine.createSpy('alert');
        module(function ($provide) {
            $provide.value('alert', alertSpy);
        });

        inject(function (myService2) {
            expect(myService2).toBe(321);
        });
    });

});